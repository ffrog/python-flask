Name:            python-flask
Version:         1.1.2
Release:         3
Epoch:           1
Summary:         A lightweight WSGI web application framework
License:         BSD
URL:             https://palletsprojects.com/p/flask/
Source0:         https://files.pythonhosted.org/packages/source/F/Flask/Flask-%{version}.tar.gz
BuildArch:       noarch

BuildRequires:   python3-devel python3-setuptools python3-pytest python3-jinja2 python3-werkzeug python3-itsdangerous python3-click

%description
Flask is a lightweight WSGI web application framework. It is designed
to make getting started quick and easy, with the ability to scale up
to complex applications. It began as a simple wrapper around Werkzeug
and Jinja and has become one of the most popular Python web application
frameworks.

%package -n python3-flask
Summary:         python-flask for python 3 version
%{?python_provide:%python_provide python3-flask}
Requires:        python3-jinja2 python3-werkzeug python3-itsdangerous python3-click

%description -n python3-flask
Python-flask for python 3 version

%prep
%autosetup -n Flask-%{version}

%build
%py3_build

%install
%py3_install
mv %{buildroot}%{_bindir}/flask{,-%{python3_version}}
ln -s flask-%{python3_version} %{buildroot}%{_bindir}/flask-3
ln -sf flask-3 %{buildroot}%{_bindir}/flask

%check
export LC_ALL=C.UTF-8
PYTHONPATH=%{buildroot}%{python3_sitelib} py.test-%{python3_version} -v || :

%files -n python3-flask
%defattr(-,root,root)
%license LICENSE*
%doc CHANGES* README*
%{_bindir}/flask
%{_bindir}/flask-3
%{_bindir}/flask-%{python3_version}
%{python3_sitelib}/*

%changelog
* Wed Oct 27 2021 Haiwei Li<lihaiwei8@huawei.com> - 1.1.2-3
- backport add require pythonx-simplejson. details see issue #I4CGIS

* Thu Sep 30 2021 Jiachen Fan<fanjiachen3@huawei.com> - 1.1.2-2
- add missing install Requires python3-simplejson

* Thu Feb 04 2021 zhaorenhai <zhaorenhai@hotmail.com> - 1.1.2-1
- Upgrade version to 1.1.2

* Mon Aug 10 2020 lingsheng<lingsheng@huawei.com> - 1.0.4-4
- Remove python2-flask subpackage

* Fri Jan 10 2020 yangjian<yangjian79@huawei.com> - 1.0.4-3
- Change the Source to valid address

* Tue Dec 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0.4-2
- Package init
